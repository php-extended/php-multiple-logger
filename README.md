# php-extended/php-multiple-logger
A psr-3 compliant logger to dispatch logs to multiple psr-3 loggers

![coverage](https://gitlab.com/php-extended/php-multiple-logger/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-multiple-logger/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-multiple-logger ^8`


## Basic Usage

This library gives a very basic psr-3 logger. Use it as described in the docs
of the psr-3 : [https://www.php-fig.org/psr/psr-3/](https://www.php-fig.org/psr/psr-3/)

```php

use PhpExtended\Logger\MultipleLogger;

$logger = new MultipleLogger(array('<logger1>', '<logger2>'));

$logger->error('Nah.');
	// dispatch $logger1->error('Nah.');
	// dispatch $logger2->error('Nah.');

```


## License

MIT (See [license file](LICENSE)).
