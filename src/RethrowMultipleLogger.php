<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-multiple-logger library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Logger;

use Exception;
use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;
use Stringable;
use Throwable;

/**
 * RethrowMultipleLogger class file.
 * 
 * This class dispatches log records to multiple loggers
 * 
 * @author Anastaszor
 */
class RethrowMultipleLogger extends AbstractLogger implements LoggerInterface, Stringable
{
	
	/**
	 * All the available loggers.
	 * 
	 * @var array<integer, LoggerInterface>
	 */
	protected array $_loggers = [];
	
	/**
	 * Builds a new MultipleLogger with the given loggers. If throwOnException
	 * is true the logger will rethrow the exception caught on any logger.
	 * 
	 * @param array<integer, LoggerInterface> $loggers
	 */
	public function __construct(array $loggers = [])
	{
		foreach($loggers as $logger)
		{
			$this->addLogger($logger);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds a logger to the available loggers.
	 * 
	 * @param LoggerInterface $logger
	 */
	public function addLogger(LoggerInterface $logger) : void
	{
		$this->_loggers[] = $logger;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Log\LoggerInterface::log()
	 * @param int|string $level
	 * @param array<string, null|bool|int|float|string|array<int|string, null|bool|int|float|string>> $context
	 * @throws Throwable
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function log($level, string|Stringable $message, array $context = []) : void
	{
		foreach($this->_loggers as $logger)
		{
			try
			{
				$logger->log($level, $message, $context);
			}
			catch(Throwable $exc)
			{
				throw $exc;
			}
		}
	}
	
}
